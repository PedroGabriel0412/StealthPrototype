﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainData
{

    public static string NextScene;
    public static bool killedGeneral;
    public static bool alarmActivated;
    public static float musicVolume;
    public static float sfxVolume;
    public static float killedEnemies;
    public static float documentos;
    public static int[] levelStatus = { 1, 0, 0, 0, 0, 0, 0, 0 };



    public static void SaveData()
    {
        PlayerPrefs.SetInt("KilledGeneral", (killedGeneral ? 1 : 0));
        PlayerPrefs.SetInt("AlarmActiveted", (alarmActivated ? 1 : 0));
        PlayerPrefs.SetFloat("MVolume", musicVolume);
        PlayerPrefs.SetFloat("MVolume", sfxVolume);
        PlayerPrefs.SetFloat("KilledEnemies", killedEnemies);
        PlayerPrefs.SetFloat("documentos", documentos);
        PlayerPrefsX.SetIntArray("levelStatus", levelStatus);
    }

    public static void LoadData()
    {
        killedGeneral = (PlayerPrefs.GetInt("KilledGeneral")!=0);
        alarmActivated = (PlayerPrefs.GetInt("AlarmActivated") != 0);
        musicVolume = PlayerPrefs.GetFloat("MVolume", 10);
        sfxVolume = PlayerPrefs.GetFloat("MVolume", 10);
        killedEnemies = PlayerPrefs.GetFloat("KilledEnemies", 0);
        if (PlayerPrefs.HasKey("levelStatus"))
        {
            levelStatus = PlayerPrefsX.GetIntArray("levelStatus");
        }
        else
        {
            levelStatus = new int[] { 1, 0, 0, 0, 0, 0, 0, 0 };
        }

        if (PlayerPrefs.HasKey("documentos"))
        {
            documentos = PlayerPrefs.GetFloat("documentos", 0);
        }
    }

    public static void ResetLevels()
    {
        levelStatus = new int[] { 1, 0, 0, 0, 0, 0, 0, 0 };
    }

    public static void ResetGeral()
    {
        PlayerPrefs.DeleteKey("KilledGeneral");
        PlayerPrefs.DeleteKey("AlarmActivated");
        PlayerPrefs.DeleteKey("KilledEnemies");
        PlayerPrefs.DeleteKey("documentos");
    }

    public static void AddEnemyKill()
    {
        killedEnemies++;
        PlayerPrefs.SetFloat("KilledEnemies", killedEnemies);
    }

    public static void GeneralKilled()
    {
        killedGeneral = true;
        SaveData();
    }

    public static void PickedDocs()
    {
        documentos++;
        PlayerPrefs.SetFloat("documentos", documentos);
    }
}

