﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoverManager : MonoBehaviour
{
    public Animator anim;
    public PlayerController playerController;
    public InteractionManager InteractionManager;
    //public UIManager uiManager;
    public Text interactionText;
    public bool inCover = false;
    public LayerMask coverMask = 1;

    private int coverIndex;
    private int standingIndex;
    private CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
        controller = GetComponent<CharacterController>();
        InteractionManager = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<InteractionManager>();

        coverIndex = Animator.StringToHash("cover");
        standingIndex = Animator.StringToHash("standing");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hitInfo, 5, coverMask) && (hitInfo.collider.CompareTag("Cover")) && hitInfo.normal.y == 0 && !inCover)
        {
            // transform.TransformDirection(hitInfo.transform.position);
            InteractionManager.canEnterCover = true;
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!inCover)
                {
                    //Quaternion angulo = transform.rotation - hitInfo.transform.rotation;
                    //Debug.Log(angulo);
                    playerController.crouching = true;
                    playerController.blockRotationPlayer = true;
                    playerController.inCover = true;
                    inCover = true;
                    controller.center = new Vector3(0, 0.9f, 0.15f);
                    //float angulo = Vector3.Angle( transform.position ,- hitInfo.transform.position);
                    transform.forward = hitInfo.normal;
                    Vector3 dislocamento = new Vector3(hitInfo.point.x, transform.position.y, hitInfo.point.z);
                    transform.position = dislocamento;//Vector3.Lerp(transform.position, dislocamento,0.1f);
                                                      //transform.rotation = Quaternion.RotateTowards(transform.rotation, hitInfo.transform.rotation, 10);
                    anim.SetTrigger(coverIndex);
                }
               
            }
        }
        else
        {
            InteractionManager.canEnterCover = false;
           // ChangeInteractionText("");
        }

        if (inCover && Input.GetKeyDown(KeyCode.E))
        {
            //Quaternion angulo = transform.rotation - hitInfo.transform.rotation;
            //Debug.Log(angulo);
            playerController.crouching = false;
            playerController.blockRotationPlayer = false;
            playerController.inCover = false;
            inCover = false;
            controller.center = new Vector3(0, 0.9f, 0.15f);
            //float angulo = Vector3.Angle( transform.position ,- hitInfo.transform.position);
            transform.forward = hitInfo.normal;
            Vector3 dislocamento = new Vector3(hitInfo.point.x, transform.position.y, hitInfo.point.z);
            transform.position = dislocamento;//Vector3.Lerp(transform.position, dislocamento,0.1f);
                                              //transform.rotation = Quaternion.RotateTowards(transform.rotation, hitInfo.transform.rotation, 10);
            anim.SetTrigger(standingIndex);
        }

}

    public void OutOfCover()
    {
        
    }

    public void ChangeInteractionText(string texto)
    {
        interactionText.text = texto;
    }
}
