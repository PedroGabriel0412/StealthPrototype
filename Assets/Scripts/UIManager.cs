﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    public Text interactionText;
    public TextMeshProUGUI secondaryObjectivesText;

    private void Awake()
    {
        MainData.LoadData();
    }

    // Start is called before the first frame update
    void Start()
    {
        ChangeSecondaryObjectivesText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeSecondaryObjectivesText()
    {
        secondaryObjectivesText.text = "Objetivos Secundarios \n - Matar todos os guardas: " + MainData.killedEnemies +" de 7\n - Coletar os documentos:" + MainData.documentos + " de 3";
    }

    public void ChangeInteractionText(string texto)
    {
        interactionText.text = texto;
    }
}
