﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectItens : MonoBehaviour
{
    //Animator anim;
    public GameObject player;

    public Text pickIndication;
    public Image docImage;
    //public GameObject objeto;
    //public int municao;
    //public float quantMunicao = 1;
    //public float maxMunicao = 3;
    //public Text textMuni;
    //public GameObject pedra;
    //public GameObject mushroom;
    //public string dialogueToCall;
    public LayerMask coverMask = 1;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        docImage.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //quantMunicao = Mathf.Clamp(quantMunicao, 0, maxMunicao);
        /*if (Input.GetKeyDown(KeyCode.F) && quantMunicao > 0)
        {
            ThrowObject();
        }*/
        /*if (quantMunicao <= 0)
        {
            textMuni.text = "";
            pedra.SetActive(false);
        }
        else if (quantMunicao == 1)
        {
            textMuni.text = "";
            pedra.SetActive(true);
        }
        else if (quantMunicao > 1)
        {
            textMuni.text = "x " + quantMushroom.ToString();
        }*/

        //Debug.Log(" E");

        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hitInfo, 10, coverMask)  && hitInfo.collider.CompareTag("Document"))
        {
            pickIndication.text = ("Press E to pick up item");
            if (Input.GetKeyDown(KeyCode.E))
            {
                docImage.enabled = true;
                Debug.Log(hitInfo.collider.gameObject.name);
                Destroy(hitInfo.collider.gameObject);
            }
            //FindObjectOfType<AudioManager>().Play("Beep Coleta");
        }
            

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(Camera.main.ScreenPointToRay(Input.mousePosition));
        Gizmos.color = Color.black;
    }

    /*public void ThrowObject()
    {
        quantMunicao--;
        GameObject lancado = Instantiate(objeto, transform.position, transform.rotation);
        lancado.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
        //Debug.Log(lancado.GetComponent<Rigidbody>());

    }*/
}
