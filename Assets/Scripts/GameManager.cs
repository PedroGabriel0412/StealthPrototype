﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject HackSistem;
    public GameObject[] AllHackCans;
    public GameObject CanAcssesTextFeddback;
    public GameObject Cam01;
    public GameObject camPointer;

    static public bool camAcsses;
    static public bool camGameplay;

    private void Update()
    {
        if (camAcsses)
        {
            Turner.TurnOn(CanAcssesTextFeddback);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ChangeGameplay();
            }
        }
        else
        {
            Turner.TurnOff(CanAcssesTextFeddback);
        }

        if (camGameplay)
        {
            Turner.TurnOn(camPointer);
        }
        else
        {
            Turner.TurnOff(camPointer);
        }
    }

    public void ChangeGameplay()
    {
        if (!camGameplay)
        {
            ChangeToCamGameplay();
        }
        else
        {
            ChangeToMapGameplay();
        }
    }

    public void ChangeToCamGameplay()
    {
        Turner.TurnOff(HackSistem);
        Turner.TurnOn(Cam01);
    }
    public void ChangeToMapGameplay()
    {
        Turner.TurnOn(HackSistem);
        TurnAllHackCamsOff();
    }

    public void TurnAllHackCamsOff()
    {
        for(int i = 0; i < AllHackCans.Length; i++)
        {
            Turner.TurnOff(AllHackCans[i]);
        }
    }
}
