﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public GameObject menu;

    private bool paused;
    private float originalFixedDeltaTime;

    // Start is called before the first frame update
    void Start()
    {
        paused = false;
        originalFixedDeltaTime = Time.fixedDeltaTime;
        menu.SetActive(false);
    }

    public void PauseGame()
    {
        menu.SetActive(true);
        paused = true;
        Time.timeScale = 0;
        Time.fixedDeltaTime = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void UnPauseGame()
    {
        menu.SetActive(false);
        paused = false;
        Time.timeScale = 1;
        Time.fixedDeltaTime = originalFixedDeltaTime;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void GoToMainMenu()
    {
        UnPauseGame();
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            if (paused)
            {
                PauseGame();
            }else
            {
                UnPauseGame();
            }
        }   
    }
}
