﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turner : MonoBehaviour
{
   static public void TurnOn(GameObject Obj)
    {
        Obj.SetActive(true);
    }
    static public void TurnOff(GameObject Obj)
    {
        Obj.SetActive(false);
    }
}
