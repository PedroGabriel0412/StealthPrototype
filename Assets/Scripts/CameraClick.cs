﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraClick : MonoBehaviour
{
    Camera cam;
    MeshRenderer CamMesh;
    BoxCollider CamCollider;

    void Start()
    {
        cam = GetComponent<Camera>();
        CamMesh = GetComponentInParent<MeshRenderer>();
        CamCollider = GetComponentInParent<BoxCollider>();
        HideCamObj();
    }

    void Update()
    {
        GameManager.camGameplay = true;
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.CompareTag("Camera"))
            {
                Transform newCamera = hit.collider.gameObject.transform.Find("RealCam");
                ShowCamObj();
                Turner.TurnOff(gameObject);
                Turner.TurnOn(newCamera.gameObject);
            }
        }
        else
            print("I'm looking at nothing!");
    }

    void HideCamObj()
    {
        CamMesh.enabled = false;
        CamCollider.enabled = false;
    }
    void ShowCamObj()
    {
        CamMesh.enabled = true;
        CamCollider.enabled = true;
    }
}
