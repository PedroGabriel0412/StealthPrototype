﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnCamAcsses : MonoBehaviour
{
    public bool isAcssesPoint;

    private void Start()
    {
        GameManager.camGameplay = true;
    }

    private void Update()
    {
        if (isAcssesPoint)
        {
            GameManager.camAcsses = true;
        }
        else
        {
            GameManager.camAcsses = false;
        }
    }

}
