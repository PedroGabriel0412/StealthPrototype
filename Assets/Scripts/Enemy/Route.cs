﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : MonoBehaviour
{
    public Transform pathHolder;

    public Vector3[] waypoints;

    public void Awake()
    {
        waypoints = new Vector3[pathHolder.childCount];
        for(int i = 0;i< waypoints.Length; i++)
        {
            waypoints[i] = pathHolder.GetChild(i).position;
            //waypoints[i] = new Vector3(waypoints.x, character.y, waypoints.z);
        }
    }

    private void OnDrawGizmos()
    {
        Vector3 startingPosition = pathHolder.GetChild(0).position;
        Vector3 previousPosition = startingPosition;

        foreach (Transform wayPoint in pathHolder)
        {
            Gizmos.DrawSphere(wayPoint.position,.3f);
            Gizmos.DrawLine(previousPosition, wayPoint.position);
            previousPosition = wayPoint.position;

        }
        Gizmos.DrawLine(previousPosition, startingPosition);
    }
}
