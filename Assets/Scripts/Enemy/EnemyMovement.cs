﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public float patrolSpeed = 2;
    public float chaseSpeed = 5;
    public float chaseWaitTime = 5;
    public float patrolWaitTime = 1;
    //public bool patrolArea;
    public Route route;

    public GameController gameController;
    [SerializeField]
    private NavMeshAgent nav;
    private float chaseTimer;
    private float patrolTimer;
    private int waypointIndex;
    private Animator anim;

    private void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
        gameController = FindObjectOfType<GameController>();
        //route = GetComponentInChildren<Route>();
        chaseTimer = 0;
        patrolTimer = 0;

    }
    

    // Start is called before the first frame update
    void Start()
    {
        if(route != null)
        StartCoroutine(FollowRoute(route.waypoints));
    }

    // Update is called once per frame
    void Update()
    {
        //Patrolling();
    }

    /// <summary>
    /// Função para fazer o inimigo procurar pelo jogador parado ou patrulanhado
    /// </summary>
    /*public void Search()
    {

    }*/

    /// <summary>
    /// metodo responsavel por fazer o inimigo patrulhar o cenario
    /// </summary>
    /*public void Patrolling()
    {
        if (route != null)
        {
            if (nav.destination == gameController.resetPosition || nav.remainingDistance < nav.stoppingDistance)
            {
                nav.speed = patrolSpeed;

                patrolTimer += Time.deltaTime;

                if (patrolTimer >= patrolWaitTime)
                {
                    if (waypointIndex ==  route.waypoints.Length - 1)
                    {
                        waypointIndex = 0;
                    }
                    else
                    {
                        waypointIndex++;
                    }
                    patrolTimer = 0;
                }
            }
            else
            {
                patrolTimer = 0;
            }

            nav.destination = route.waypoints[waypointIndex];
        }
    }*/

    IEnumerator FollowRoute(Vector3[] waypoints)
    {
        transform.position = waypoints[0];

        //int targetWaypointIndex = 1;
        waypointIndex = 1;
        Vector3 targetWaypoint = waypoints[waypointIndex];
        transform.LookAt(targetWaypoint);

        while (true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetWaypoint, patrolSpeed * Time.deltaTime);
            if (transform.TransformPoint(transform.position) == targetWaypoint || Vector3.Distance(transform.position, targetWaypoint) < 1.5f)
            {
                waypointIndex = (waypointIndex +1) % waypoints.Length;
                targetWaypoint = waypoints[waypointIndex];
                yield return new WaitForSeconds(patrolWaitTime);
                yield return StartCoroutine(TurnToFace(targetWaypoint));
            }
            yield return null;
        }
    }

    IEnumerator TurnToFace(Vector3 lookTarget)
    {
        Vector3 dirToLookTarget = (lookTarget - transform.position).normalized;
        float targetAngle = 90 - Mathf.Atan2(dirToLookTarget.z, dirToLookTarget.x) * Mathf.Rad2Deg;

        while (Mathf.Abs(Mathf.DeltaAngle(transform.eulerAngles.y,targetAngle)) > 0.05f)
        {
            float angle = Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetAngle, chaseSpeed * Time.deltaTime);
            transform.eulerAngles = Vector3.up * angle;
            yield return null;
        }
    }
}

