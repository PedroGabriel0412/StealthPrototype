﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealthCheck : MonoBehaviour
{
    public PlayerController playerController;
    public InteractionManager InteractionManager;
    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<PlayerController>();
        InteractionManager = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<InteractionManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Aqui ele tá " +  other.gameObject.name +" tag " + other.gameObject.tag);
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("leu o meio");
            playerController.canStealthKill = true;
            InteractionManager.canStealthKill = true;
            //InteractionManager.ChangeInteractionText("Press E to stealth kill");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Mas saiu tb");
            playerController.canStealthKill = false;
            InteractionManager.canStealthKill = false;
            //InteractionManager.ChangeInteractionText("");
        }
    }
}
