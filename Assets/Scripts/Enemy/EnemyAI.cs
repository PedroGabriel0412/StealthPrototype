﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{

   // public float chaseSpeed = 5;
    /// <summary>varialel para dizer se o inimigo de ve se mobimentar ou não</summary>
    public bool patrolArea;
    /// <summary>varialel para saber se o inimigo vê o player ou não</summary>
    public bool playerVisible;

    protected FieldOfView enemySight;
    private EnemyMovement enemyMovement;
    public GameController lastPlayerPos;
    /// <summary>variavel que diz qual a ação realizada no momento pelol inimigo</summary>
    public EnemyState _currentState;
    /// <summary>Enum contendo os estados  que o inimigo pode ficar</summary>
    public enum EnemyState
    {
        Patrol,
        Chase,
        Attack
    }

    // Start is called before the first frame update
    void Awake()
    {
        enemySight = GetComponent<FieldOfView>();
        enemyMovement = GetComponent<EnemyMovement>();
        //nav = GetComponent<NavMeshAgent>();
        //anim = GetComponent<Animator>();
        //playerHealth = player.GetComponent<PlayerHealth>();
        lastPlayerPos = FindObjectOfType<GameController>();
        //gameUiManager = lastPlayerSighting.GetComponent<UIManager>();
        //deathIndex = Animator.StringToHash("death");

    }

    private void Update()
    {
        switch (_currentState)
        {
            case EnemyState.Patrol:
                // escrever o q o patrol faz aqui
                break;
            case EnemyState.Chase:
                // escrever o q o chase faz aqui
                if (!playerVisible && enemySight.playerVisibleTimer <= 0)
                {
                    _currentState = EnemyState.Patrol;
                }
                break;
            case EnemyState.Attack:
                // escrever o q o attack faz aqui
                break;
        }

    }

    /*void Patrolling()
    {
        if (patrolWayPoints.Length > 0)
        {
            //anim.SetBool("move", true);
        //nav.speed = patrolSpeed;

        if (nav.destination == lastPlayerPos.resetPosition || nav.remainingDistance < nav.stoppingDistance)
        {
            patrolTimer += Time.deltaTime;

            if (patrolTimer >= patrolWaitTime)
            {
                if (wayPointIndex == patrolWayPoints.Length -1)
                {
                    wayPointIndex = 0;
                } else
                {
                    wayPointIndex++;
                }
                patrolTimer = 0;
            }
        }
        else
        {
            patrolTimer = 0;
        }

            //nav.destination = patrolWayPoints[wayPointIndex].position;
        }
        //print(nav.destination);
    }*/

    public virtual void Death()
    {
        //nav.isStopped = true;
        //anim.SetTrigger(deathIndex);
    }

    /*public virtual void Dying()
    {
        MainData.AddEnemyKill();
        gameUiManager.ChangeSecondaryObjectivesText();
        Destroy(gameObject);
    }*/
}
