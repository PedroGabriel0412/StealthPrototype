﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    /// <summary>VAriavel para ter acesso ao game controller e seus dados</summary>
    public GameController gameController;
    public EnemyAI enemyAI;
    public float viewRaidius;
    [Range(0,360)]
    public float viewAngle;
    /// <summary>Layer mask para objetos como o player que querem ser vistos</summary>
    public LayerMask targetMask;
    /// <summary>varialel para objetos que obstruam a visão</summary>
    public LayerMask obstacleMask;

    [HideInInspector]
    public List<Transform> visibleTargets = new List<Transform>();
    /// <summary>Variaveis respnsaveis pelos fatores que regulam o mesh criado pelo campo de visão</summary>
    public float meshResolution;
    public int edgeResolveInterations;
    public float edgeDistanceThreshold;
    public bool renderVision;
    //public bool canSeePlayer;
    /// <summary>variaveis que são usadas para a detecção do player</summary>
    public float viewDistance;
    public float playerVisibleTimer;
    public float timeToSpotPlayer;
    public Light spotlight;
    public Color originalSpotlightColor;

    public MeshFilter viewMeshFilter;
    Mesh viewMesh;
    private Transform player;

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }
    
    bool CanSeePlayer()
    {
        if (Vector3.Distance(transform.position, player.position) < viewDistance)
        {
            Vector3 dirToPlayer = (player.position - transform.position).normalized;
            float angleBetweenEnemyAndPlayer = Vector3.Angle(transform.forward, dirToPlayer);
            if(angleBetweenEnemyAndPlayer < viewAngle / 2) {
                if (!Physics.Linecast(transform.position,player.position,obstacleMask))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        enemyAI = FindObjectOfType<EnemyAI>();
        player = gameController.playerTransform;

    }

    private void Start()
    {
        if (spotlight != null)
        {
            originalSpotlightColor = spotlight.color;
            viewAngle = spotlight.spotAngle;
        }
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;
        StartCoroutine("FindTargetsWithDelay", .2f);
    }

    private void Update()
    {
        if (CanSeePlayer())
        {
            playerVisibleTimer += Time.deltaTime;
            //spotlight.color = Color.red;
        }
        else
        {
            playerVisibleTimer -= Time.deltaTime;
            //spotlight.color = originalSpotlightColor;
        }
        playerVisibleTimer = Mathf.Clamp(playerVisibleTimer, 0, timeToSpotPlayer);
        spotlight.color = Color.Lerp(originalSpotlightColor, Color.yellow, playerVisibleTimer / timeToSpotPlayer);

        if(playerVisibleTimer >= timeToSpotPlayer)
        {
            spotlight.color = Color.red;
        }
        enemyAI.playerVisible = CanSeePlayer();
    }

    private void LateUpdate()
    {
        if (renderVision)
        {
            DrawFieldOfView();
        }
        else
        {
            if(viewMesh.vertexCount > 1)
            {
                viewMesh.Clear();
            }
        }
    }

    void FindVisibleTargets()
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRaidius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if(Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstTOTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position,dirToTarget,dstTOTarget,obstacleMask))
                {
                    //if (target.gameObject.CompareTag("Player")) {
                        visibleTargets.Add(target);
                    //}
                }

            }
           
        }

    }

    void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for(int i = 0; i <= stepCount; i++)
        {
            float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);

            if(i > 0)
            {
                bool edgeDistanceThresholdExceeded = Mathf.Abs(oldViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;

                if (oldViewCast.hit != newViewCast.hit ||  (oldViewCast.hit && newViewCast.hit && edgeDistanceThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if(edge.pointA != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero)
                    {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for(int i = 0; i<vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

            if(i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }
        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();

     }

    EdgeInfo FindEdge(ViewCastInfo minViewCast,ViewCastInfo MaxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = MaxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for(int i = 0;i < edgeResolveInterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newviewCast = ViewCast(angle);
            bool edgeDisttanceThresholdExceeded = Mathf.Abs(minViewCast.distance - MaxViewCast.distance) > edgeDistanceThreshold;

            if (newviewCast.hit == minViewCast.hit && !edgeDisttanceThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newviewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newviewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit Hit;

        if(Physics.Raycast(transform.position, dir,out Hit, viewRaidius, obstacleMask))
        {
            return new ViewCastInfo(true, Hit.point, Hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(true, transform.position + dir * viewRaidius, viewRaidius, globalAngle);
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),0,Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float distance;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _distance, float _angle)
        {
            hit = _hit;
            point = _point;
            distance = _distance;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        //maxPoint
        public Vector3 pointA;
        //minPoint
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }
}
