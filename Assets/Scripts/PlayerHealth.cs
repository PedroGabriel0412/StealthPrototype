﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float health = 100f;
    public float resetAfterDeathTime = 5f;
    //public AudioClip deathClip;

    private Animator anim;
    private PlayerController playerController;
    private HashIDs hash;
    //private SceneFadeInOut 
    private LastPlayerSighting lastPlayerSighting;
    private float timer;
    private bool playerDead;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
        hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
        //sceneFadeInOut = GameObject.FindGameObjectWithTag(Tags.fader).GetComponent(SceneFadeInOut);
        lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<LastPlayerSighting>();
    }

    private void Update()
    {
        if(health <= 0)
        {
            if (!playerDead) {
                PlayerDying();
            }else
            {
                PlayerDead();
                ResetLevel();
            }

        }
    }

    void PlayerDying()
    {
        playerDead = true;
        // trigger da animação de morteanimação de morte e tocar o som de morte

    }

    void PlayerDead()
    {
        /*if (Layer e estado atual da animação paara saber se esta no de morte)
        {
            fazer a animação de morte só tocar uma vez
            bool dead trocar o sinal
        }*/
        playerController.enabled = false;
        lastPlayerSighting.position = lastPlayerSighting.resetPosition;

    }


    void ResetLevel()
    {
        timer += Time.deltaTime;
        if(timer>= resetAfterDeathTime)
        {
          //  sceneFadeInOut.EndScene();
        }
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
    }
}
