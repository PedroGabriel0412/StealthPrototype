﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackNavigation : MonoBehaviour
{
    public int speed;

    Rigidbody rgb;
    bool moving = false;

   

    private void Start()
    {
        rgb = gameObject.GetComponent<Rigidbody>();
    }
    private void Update()
    {
        GameManager.camGameplay = false;
        if (Input.GetKeyDown(KeyCode.RightArrow) && !moving)
        {
            rgb.velocity = new Vector3(speed, 0, 0);
            moving = true;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && !moving)
        {
            rgb.velocity = new Vector3(-speed, 0, 0);
            moving = true;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && !moving)
        {
            rgb.velocity = new Vector3(0, speed, 0);
            moving = true;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && !moving)
        {
            rgb.velocity = new Vector3(0, -speed, 0);
            moving = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("HTravlePoint") || other.CompareTag("HCamPoint"))
        {
            rgb.velocity = new Vector3(0, 0, 0);
            transform.position = new Vector3(
                other.transform.position.x,
                other.transform.position.y,
                transform.position.z );
            moving = false;

            if (other.CompareTag("HCamPoint"))
                GameManager.camAcsses = true;
            else
                GameManager.camAcsses = false;
        }
    }
}
