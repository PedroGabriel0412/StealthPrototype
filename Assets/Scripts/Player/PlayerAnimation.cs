﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator playerAnim;

    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;
    //float
    [HideInInspector]
    public int speedIndex;
    //bool
    [HideInInspector]
    public int moveIndex;
    [HideInInspector]
    public int sprintIndex;
    //trigger
    [HideInInspector]
    public int meleeAttackIndex;
    [HideInInspector]
    public int rangedAttackIndex;
    [HideInInspector]
    public int damageIndex;
    [HideInInspector]
    public int deadIndex;
    [HideInInspector]
    public int crouchIndex;
    [HideInInspector]
    public int standIndex;
    [HideInInspector]
    public int coverIndex;
    [HideInInspector]
    public int stealthKillIndex;

    private void Awake()
    {
        playerAnim =  GetComponentInChildren<Animator>();
        speedIndex = Animator.StringToHash("speed");
        moveIndex = Animator.StringToHash("move");
        sprintIndex = Animator.StringToHash("sprint");
        meleeAttackIndex = Animator.StringToHash("meleeAttack");
        rangedAttackIndex = Animator.StringToHash("rangedAttack");
        damageIndex = Animator.StringToHash("damage");
        deadIndex = Animator.StringToHash("dead");
        crouchIndex = Animator.StringToHash("crouch");
        standIndex = Animator.StringToHash("stand");
        coverIndex = Animator.StringToHash("cover");
        stealthKillIndex = Animator.StringToHash("stealthKill");

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
