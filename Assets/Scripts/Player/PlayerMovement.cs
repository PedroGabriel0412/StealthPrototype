﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public PlayerAnimation playerAnimation;

    [Header("Controle de Movimento")]
    public float InputX;
    public float InputZ;
    public Vector3 desiredMoveDirection;
    public bool blockRotationPlayer;
    public float speed;
    public float desiredRotationSpeed = 0.1f;
    public float allowPlayerRotation = 0.1f;
    public bool crouching;
    public bool cantStandUp;
    public LayerMask layerMask;
    public Camera cam;

    private CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        playerAnimation = GetComponent<PlayerAnimation>();
        controller = GetComponent<CharacterController>();
        crouching = false;
        cantStandUp = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Crouch();
        }
    }

    void LateUpdate()
    {
        //Calculate Input Vectors
        InputX = Input.GetAxis("Horizontal");
        InputZ = Input.GetAxis("Vertical");

        speed = new Vector2(InputX, InputZ).sqrMagnitude;
        InputMagnitude();
        cantStandUp = Physics.Raycast(transform.position, Vector3.up, 2f,layerMask);
    }

    /// <summary>
    /// Função responsavel pela rotação do player 
    /// </summary>
    void PlayerMoveAndRotation()
    {
       
        //bool s = Input.GetKey(KeyCode.LeftShift) ? true : false;

        var forward = cam.transform.forward;
        var right = cam.transform.right;
        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * InputZ + right * InputX;

        if ((InputX > 0 && Vector3.Angle(cam.transform.right, gameObject.transform.forward) < 10) || (InputX < 0 && Vector3.Angle(-cam.transform.right, gameObject.transform.forward) < 10))
        {
            blockRotationPlayer = false;
        }
      

        if (blockRotationPlayer == false )
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
        }
        //playerAnimation.playerAnim.SetBool(playerAnimation.sprintIndex, s);
    }

    /// <summary>
    /// Função que aplica na movimentãção os input 
    /// </summary>
    void InputMagnitude()
    {
        bool s = Input.GetKey(KeyCode.LeftShift) ? true : false;
        //Physically move player
        if (speed > allowPlayerRotation)
        {
            playerAnimation.playerAnim.SetFloat(playerAnimation.speedIndex, Mathf.Clamp(speed, -1, 1), playerAnimation.StartAnimTime, Time.deltaTime);
            PlayerMoveAndRotation();
            playerAnimation.playerAnim.SetBool(playerAnimation.sprintIndex, s);
        }
        else if (speed < allowPlayerRotation)
        {
            playerAnimation.playerAnim.SetFloat(playerAnimation.speedIndex, Mathf.Clamp(speed, -1, 1), playerAnimation.StopAnimTime, Time.deltaTime);
        }
        playerAnimation.playerAnim.SetBool(playerAnimation.moveIndex, Mathf.Abs(speed) >= 0.2F);
    }


    /// <summary>
    /// Função responsavel pelo agachar e levantar do player
    /// </summary>
    private void Crouch()
    {
        

        if (!crouching)
        {
            crouching = !crouching;
            playerAnimation.playerAnim.SetBool(playerAnimation.crouchIndex,true);
            controller.height = 0.9f;
            controller.center = new Vector3(controller.center.x, 0.45f, controller.center.z);
        }
        else if(crouching && !cantStandUp)
        {
            controller.height = 1.8f;
            controller.center = new Vector3(controller.center.x, 0.9f, controller.center.z);
            crouching = !crouching;
            playerAnimation.playerAnim.SetBool(playerAnimation.crouchIndex, false);
        }
    }
}


