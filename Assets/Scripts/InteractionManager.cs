﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionManager : MonoBehaviour
{
    //private Animator anim;
    public PlayerController playerController;
    public UIManager gameUIManager;
    public Text interactionText;
    public Image docImage;
    public LayerMask coverMask = 1;
    public bool canStealthKill;
    public bool canEnterCover;

    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<PlayerController>();
        gameUIManager = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<UIManager>();
        docImage.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (!canStealthKill)
        {*/
            //Raycast geral
            RaycastHit hitInfo;
        //Raycast do stealth
        RaycastHit hitInfo2;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (canEnterCover)
        {
            gameUIManager.ChangeInteractionText("Press E to enter cover");
            canEnterCover = true;
        }
        else if (Physics.Raycast(ray, out hitInfo, 10, coverMask) && hitInfo.collider.CompareTag("Document"))
        {
            if (hitInfo.collider.CompareTag("Document"))
            {
                gameUIManager.ChangeInteractionText("Press E to pick up item");
                if (Input.GetKeyDown(KeyCode.E))
                {
                    //docImage.enabled = true;
                    MainData.PickedDocs();
                    gameUIManager.ChangeSecondaryObjectivesText();
                    Debug.Log(hitInfo.collider.gameObject.name);
                    Destroy(hitInfo.collider.gameObject);
                }
            }
        }
        else if (Physics.Raycast(ray, out hitInfo2, 10, coverMask) && hitInfo2.collider.CompareTag("Enemy"))
        {
            EnemySight eSight = hitInfo.collider.gameObject.GetComponentInParent<EnemySight>();
            if (eSight.playerInSight == false)
            {
                playerController.canStealthKill = true;
                canStealthKill = true;
                gameUIManager.ChangeInteractionText("Press E to stealth kill");
                if (Input.GetKeyDown(KeyCode.E) && canStealthKill)
                {
                    EnemyAI enemyAI = eSight.GetComponent<EnemyAI>();
                    enemyAI.Death();
                    canStealthKill = false;
                    playerController.canStealthKill = false;

                    playerController.StealthKill(); ;
                }

            }

        }
        else
        {
            playerController.canStealthKill = false;
            canStealthKill = false;
            gameUIManager.ChangeInteractionText("");
        }
            //}
        
    }

        /*public void ChangeInteractionText(string texto)
        {
            interactionText.text = texto;
        }*/
    }
