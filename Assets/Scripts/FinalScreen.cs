﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FinalScreen : MonoBehaviour
{

    public TextMeshProUGUI resultGeneralText;
    public TextMeshProUGUI resultEnemiesText;
    public TextMeshProUGUI resultDocumentsText;
    public TextMeshProUGUI resultAlarmText;
    public TextMeshProUGUI finalResult;
    private float resultValue;

    private void Awake()
    {
        MainData.LoadData();
    }

    // Start is called before the first frame update
    void Start()
    {
        CalculateGeneralValue();
        CalculateEnemiesValue();
        CalculateDocumentsValue();
        CalculateAlarmText();
        CalculateRanking();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CalculateGeneralValue()
    {
        if (MainData.killedGeneral)
        {
            resultGeneralText.text = "Concluida - 200 pontos";
            resultValue += 200;
        } else
        {
            resultGeneralText.text = "Fracassada - 0 pontos";
        }
    }

    private void CalculateEnemiesValue()
    {
        if (MainData.killedEnemies == 7)
        {
            resultEnemiesText.text = "Concluida - 500 pontos";
            resultValue += 500;
        }
        else
        {
            resultEnemiesText.text = "Fracassada - " + 500 * (MainData.killedEnemies/7) + " pontos";
            resultValue += 500 * (MainData.killedEnemies / 7);
        }
    }

    private void CalculateDocumentsValue()
    {
        if (MainData.documentos == 3)
        {
            resultDocumentsText.text = "Concluida - 200 pontos";
            resultValue += 200;
        }
        else
        {
            resultDocumentsText.text = "Fracassada - " + 200 * (MainData.documentos/3) + " pontos";
            resultValue += 200 * (MainData.documentos / 3);
        }
    }

    private void CalculateAlarmText()
    {
        if (MainData.alarmActivated)
        {
            resultAlarmText.text = "Concluida - 100 pontos";
            resultValue += 100;
        }
        else
        {
            resultAlarmText.text = "Fracassada - 0 pontos";
        }
    }

    private void CalculateRanking()
    {
        if (resultValue == 1000)
        {
            finalResult.text = "Titulo: Sombra Assassina " + resultValue;
        }
        else if (1000 > resultValue && resultValue > 700)
        {
            finalResult.text = "Titulo: Ronin de Renome " + resultValue;
        }
        else if (700 > resultValue && resultValue > 500)
        {
            finalResult.text = "Titulo: Assasino de Aluguel " + resultValue;
        }
        else {
            finalResult.text = "Titulo: Ninja Sem Mestre " + resultValue + " pontos";
        }
    }
}
