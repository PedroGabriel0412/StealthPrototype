﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private GameController Instance;

    public Transform playerTransform;
    public Vector3 resetPosition = new Vector3(1000f, 1000f, 1000f);
    public Vector3 lastKnownPosition = new Vector3(1000f, 1000f, 1000f);
    public enum gameState
    {
        Calm,
        Alerted,
        Alarmed
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
